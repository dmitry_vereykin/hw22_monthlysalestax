/**
 * Created by Dmitry on 7/15/2015.
 */
import javax.swing.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class SalesTax extends JFrame {
    private JPanel panel;
    private JTextField totalSales;
    private JButton calcButton;

    private final int WINDOW_WIDTH = 360;
    private final int WINDOW_HEIGHT = 100;

    private final double COUNTY_TAX_RATE = 0.02;
    private final double STATE_TAX_RATE = 0.04;

    public SalesTax() {
        setTitle("Monthly Sales Tax Reporter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buildPanel();
        add(panel);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setVisible(true);
    }

    private void buildPanel(){
        JLabel totalSalesMsg = new JLabel("Enter the total sales:");

        totalSales = new JTextField(10);

        calcButton = new JButton("Calculate Sales Tax");
        calcButton.addActionListener(new CalcButtonListener());

        panel = new JPanel();
        panel.add(totalSalesMsg);
        panel.add(totalSales);
        panel.add(calcButton);
    }

    private class CalcButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double totalSalesAmount, countyTaxAmount, stateTaxAmount, totalTaxAmount;

            DecimalFormat dollar = new DecimalFormat("#,##0.00");

            try {
                totalSalesAmount = Double.parseDouble(totalSales.getText());
            } catch (Exception exc) {
                totalSalesAmount = 0.0;
                JOptionPane.showMessageDialog(null, "ERROR. Entered value is not a number.");
            }

            if (totalSalesAmount > 0) {
                countyTaxAmount = totalSalesAmount * COUNTY_TAX_RATE;
                stateTaxAmount = totalSalesAmount * STATE_TAX_RATE;
                totalTaxAmount = countyTaxAmount + stateTaxAmount;

                JOptionPane.showMessageDialog(null, "County Sales Tax; $" + dollar.format((countyTaxAmount)) +
                                                                                    "\nState Sales Tax: $" + dollar.format(stateTaxAmount) +
                                                                                    "\nTotal Sales Tax: $" + dollar.format(totalTaxAmount));
            } else if (totalSalesAmount < 0) {
                JOptionPane.showMessageDialog(null, "ERROR. Entered value is negative.");
            }
        }
    }

    public static void main(String[] args){
        new SalesTax();
    }
}
